package edu.url.salle.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import edu.url.salle.model.TipoCarne;

@Repository
public interface TipoCarneRepository extends JpaRepository< TipoCarne, Integer>{

}
